import { INavData } from '@coreui/angular';

export const nav2: INavData[] = [
  {
    title: true,
    name: 'Funciones'
  },
  {
    name: 'Gestion de expedientes',
    url: '/expedientes',
    icon: 'cil-library',
    children: [
      {
        name: 'Listado de expedientes',
        url: '/expedientes/lista',
        icon: 'cil-library'
      },
      {
        name: 'Busqueda de expediente',
        url: '/expedientes/busqueda',
        icon: 'cil-library'
      },
      {
        name: 'Partes',
        url: '/expedientes/registro-parte',
        icon: 'cil-user'
      },
      {
        name: 'Pruebas',
        url: '/expedientes/registro-prueba',
        icon: 'cil-library'
      },
    ]
  },
  ];

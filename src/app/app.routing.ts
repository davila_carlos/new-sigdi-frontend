import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BusquedaExpedienteComponent } from './components/expedientes/busqueda-expediente/busqueda-expediente.component'
import { DetalleExpedienteComponent } from './components/expedientes/detalle-expediente/detalle-expediente.component';
import { ListaExpedientesComponent } from './components/expedientes/lista-expedientes/lista-expedientes.component';
import { RegistrarExpedienteComponent } from './components/expedientes/registrar-expediente/registrar-expediente.component';
import { DetalleParteComponent } from './components/partes/detalle-parte/detalle-parte.component'
import { ListaPartesComponent } from './components/partes/lista-partes/lista-partes.component'
import { RegistrarParteComponent } from './components/partes/registrar-parte/registrar-parte.component';
import { DetallePruebaComponent } from './components/pruebas/detalle-prueba/detalle-prueba.component'
import { ListaPruebasComponent } from './components/pruebas/lista-pruebas/lista-pruebas.component'
import { RegistrarPruebaComponent } from './components/pruebas/registrar-prueba/registrar-prueba.component';
import { BajaUsuarioComponent } from './components/usuarios/baja-usuario/baja-usuario.component';
import { CambioPasswordUsuarioComponent } from './components/usuarios/cambio-password-usuario/cambio-password-usuario.component';
import { ListaUsuarioComponent } from './components/usuarios/lista-usuario/lista-usuario.component';
import { PerfilUsuarioComponent } from './components/usuarios/perfil-usuario/perfil-usuario.component';
import { RegistrarUsuarioComponent } from './components/usuarios/registrar-usuario/registrar-usuario.component';

// Import Containers
import { DefaultLayoutComponent } from './containers';
import { AuthGuard } from './guards/auth.guard';

import { P404Component } from './views/error/404.component';
import { P500Component } from './views/error/500.component';
import { LoginComponent } from './views/login/login.component';
import { RegisterComponent } from './views/register/register.component';

export const routes: Routes = [
  {
    path: '',
    redirectTo: 'dashboard',
    pathMatch: 'full',
  },
  {
    path: '404',
    component: P404Component,
    data: {
      title: 'Page 404'
    }
  },
  {
    path: '500',
    component: P500Component,
    data: {
      title: 'Page 500'
    }
  },
  {
    path: 'login',
    component: LoginComponent,
    data: {
      title: 'Login Page'
    }
  },
  {
    path: 'register',
    component: RegisterComponent,
    data: {
      title: 'Register Page'
    }
  },
  {
    path: '',
    component: DefaultLayoutComponent,
    canActivate: [AuthGuard],
    data: {
      title: 'Home'
    },
    children: [
      {
        path: 'base',
        loadChildren: () => import('./views/base/base.module').then(m => m.BaseModule)
      },
      {
        path: 'buttons',
        loadChildren: () => import('./views/buttons/buttons.module').then(m => m.ButtonsModule)
      },
      {
        path: 'charts',
        loadChildren: () => import('./views/chartjs/chartjs.module').then(m => m.ChartJSModule)
      },
      {
        path: 'dashboard',
        loadChildren: () => import('./views/dashboard/dashboard.module').then(m => m.DashboardModule),
        
      },
      {
        path: 'icons',
        loadChildren: () => import('./views/icons/icons.module').then(m => m.IconsModule)
      },
      {
        path: 'notifications',
        loadChildren: () => import('./views/notifications/notifications.module').then(m => m.NotificationsModule)
      },
      {
        path: 'theme',
        loadChildren: () => import('./views/theme/theme.module').then(m => m.ThemeModule)
      },
      {
        path: 'widgets',
        loadChildren: () => import('./views/widgets/widgets.module').then(m => m.WidgetsModule)
      },
      {
        path: 'usuarios/registro',
        component: RegistrarUsuarioComponent,
        data: {
          title: 'Registrar usuario'
        },
      },
      {
        path: 'usuarios/lista',
        component: ListaUsuarioComponent,
        data: {
          title: 'Lista de usuarios'
        }
      },
      {
        path: 'usuarios/eliminar',
        component: BajaUsuarioComponent,
        data: {
          title: 'Baja de usuario'
        }
      },
      {
        path: 'usuario/perfil',
        component: PerfilUsuarioComponent,
        data: {
          title: 'Perfil de usuario'
        }
      },
      {
        path: 'usuario/cambio-password',
        component: CambioPasswordUsuarioComponent,
        data: {
          title: 'Cambio de Contraseña'
        }
      },
      {
        path: 'expedientes/lista',
        component: ListaExpedientesComponent,
        data: {
          title: 'Lista de expedientes'
        }
      },
      {
        path: 'expedientes/detalle',
        component: DetalleExpedienteComponent,
        data: {
          title: 'Detalle de expediente'
        }
      },
      {
        path: 'expedientes/registro',
        component: RegistrarExpedienteComponent,
        data: {
          title: 'Cambio de Contraseña'
        }
      },
      {
        path: 'expedientes/registro-prueba',
        component: RegistrarPruebaComponent,
        data: {
          title: 'Cambio de Contraseña'
        }
      },
      {
        path: 'expedientes/registro-parte',
        component: RegistrarParteComponent,
        data: {
          title: 'Cambio de Contraseña'
        }
      },
      {
        path: 'expedientes/busqueda',
        component: BusquedaExpedienteComponent,
        data: {
          title: 'Busqueda de expedientes'
        }
      },
      {
        path: 'partes/detalle-parte',
        component: DetalleParteComponent,
        data: {
          title: 'Detalle Parte'
        }
      },
      {
        path: 'pruebas/detalle-prueba',
        component: DetallePruebaComponent,
        data: {
          title: 'Detalle Prueba'
        }
      }
    ]
  },
  { path: '**', component: P404Component }
];

@NgModule({
  imports: [ RouterModule.forRoot(routes, { relativeLinkResolution: 'legacy' }) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}

import { INavData } from '@coreui/angular';

export const navItems: INavData[] = [
  {
    title: true,
    name: 'Funciones'
  },
  {
    name: 'Gestion de usuarios',
    url: '/usuarios',
    icon: 'icon-user',
    children: [
      {
        name: 'Registrar usuarios',
        url: '/usuarios/registro',
        icon: 'cil-user-plus'
      },
      {
        name: 'Lista de usuarios',
        url: '/usuarios/lista',
        icon: 'cil-list-rich'
      },
      {
        name: 'Baja de usuarios',
        url: '/usuarios/eliminar',
        icon: 'cil-user-x'
      },
    ]
  },
  ];

import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { Router } from "@angular/router";
import { ToastrService } from "ngx-toastr";
import { LoginUsuario } from "../../models/loginUsuario.model";
import { UsuarioLogged } from "../../models/usuarioLogged.model";
import { LoginService } from "../../services/login.service";

@Component({
  selector: "app-dashboard",
  templateUrl: "login.component.html",
  styleUrls: ["./login.component.scss"],
})
export class LoginComponent implements OnInit {
  typePassword: boolean = false;
  loginForm: FormGroup;
  datosLoginUsuario: LoginUsuario;
  usuarioLogged: UsuarioLogged;

  constructor(
    private fb: FormBuilder,
    private loginService: LoginService,
    private toastr: ToastrService,
    private router: Router
  ) {
    this.loginForm = this.fb.group({
      username: ["", Validators.required],
      password: ["", Validators.required],
    });
  }

  ngOnInit() {}

  login() {
    if (this.loginForm.valid) {
      const datosUsuario = (this.datosLoginUsuario = {
        username: this.loginForm.get("username").value,
        userpass: this.loginForm.get("password").value,
      });
      console.log(datosUsuario);
      this.loginService.login(datosUsuario).subscribe(
        (data) => {
          console.log(data);
          this.usuarioLogged = data;
          if (this.usuarioLogged.resultado) {
            localStorage.setItem('username', datosUsuario.username)
            localStorage.setItem('userpass', datosUsuario.userpass)
            this.router.navigate(['/'])
          } else {
            this.toastr.info(
              "El usuario o la contraseña son incorrectas",
              "Error de logueo"
            );
          }
        },
        (error) => {
          console.log(error);
          this.loginForm.reset();
        }
      );
    } else {
      Object.keys(this.loginForm.controls).forEach((field) => {
        // {1}
        const control = this.loginForm.get(field) as any; // {2}
        control.markAsTouched({ onlySelf: true }); // {3}
      });
    }
  }

  togglePassword() {
    this.typePassword = !this.typePassword;
  }
}

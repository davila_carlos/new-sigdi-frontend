import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { LocationStrategy, HashLocationStrategy } from '@angular/common';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { PERFECT_SCROLLBAR_CONFIG } from 'ngx-perfect-scrollbar';
import { PerfectScrollbarConfigInterface } from 'ngx-perfect-scrollbar';

import { IconModule, IconSetModule, IconSetService } from '@coreui/icons-angular';

const DEFAULT_PERFECT_SCROLLBAR_CONFIG: PerfectScrollbarConfigInterface = {
  suppressScrollX: true
};

import { AppComponent } from './app.component';

// Import containers
import { DefaultLayoutComponent } from './containers';

import { P404Component } from './views/error/404.component';
import { P500Component } from './views/error/500.component';
import { LoginComponent } from './views/login/login.component';
import { RegisterComponent } from './views/register/register.component';

const APP_CONTAINERS = [
  DefaultLayoutComponent
];

import {
  AppAsideModule,
  AppBreadcrumbModule,
  AppHeaderModule,
  AppFooterModule,
  AppSidebarModule,
} from '@coreui/angular';

// Import routing module
import { AppRoutingModule } from './app.routing';

// Import 3rd party components
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { ChartsModule } from 'ng2-charts';
import { RegistrarUsuarioComponent } from './components/usuarios/registrar-usuario/registrar-usuario.component';
import { ListaUsuarioComponent } from './components/usuarios/lista-usuario/lista-usuario.component';
import { BajaUsuarioComponent } from './components/usuarios/baja-usuario/baja-usuario.component';
import { PerfilUsuarioComponent } from './components/usuarios/perfil-usuario/perfil-usuario.component';
import { CambioPasswordUsuarioComponent } from './components/usuarios/cambio-password-usuario/cambio-password-usuario.component';
import { ReactiveFormsModule } from '@angular/forms';
import { ToastrModule } from 'ngx-toastr';
import { HttpClientModule} from '@angular/common/http';
import { ListaExpedientesComponent } from './components/expedientes/lista-expedientes/lista-expedientes.component';
import { RegistrarExpedienteComponent } from './components/expedientes/registrar-expediente/registrar-expediente.component';
import { DetalleExpedienteComponent } from './components/expedientes/detalle-expediente/detalle-expediente.component';
import { RegistrarParteComponent } from './components/partes/registrar-parte/registrar-parte.component';
import { RegistrarPruebaComponent } from './components/pruebas/registrar-prueba/registrar-prueba.component'
import { ListaPartesComponent } from './components/partes/lista-partes/lista-partes.component'
import { ListaPruebasComponent } from './components/pruebas/lista-pruebas/lista-pruebas.component';
import { DetalleParteComponent } from './components/partes/detalle-parte/detalle-parte.component';
import { DetallePruebaComponent } from './components/pruebas/detalle-prueba/detalle-prueba.component';
import { BusquedaExpedienteComponent } from './components/expedientes/busqueda-expediente/busqueda-expediente.component';
import { DatosExpedienteComponent } from './components/expedientes/datos-expediente/datos-expediente.component'
import { ListaActuadosComponent } from './components/actuados/lista-actuados/lista-actuados.component';

@NgModule({
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    AppAsideModule,
    AppBreadcrumbModule.forRoot(),
    AppFooterModule,
    AppHeaderModule,
    AppSidebarModule,
    PerfectScrollbarModule,
    BsDropdownModule.forRoot(),
    TabsModule.forRoot(),
    ChartsModule,
    IconModule,
    IconSetModule.forRoot(),
    ReactiveFormsModule,
    ToastrModule.forRoot(),
    HttpClientModule,
  ],
  declarations: [
    AppComponent,
    ...APP_CONTAINERS,
    P404Component,
    P500Component,
    LoginComponent,
    RegisterComponent,
    RegistrarUsuarioComponent,
    ListaUsuarioComponent,
    BajaUsuarioComponent,
    PerfilUsuarioComponent,
    CambioPasswordUsuarioComponent,
    ListaExpedientesComponent,
    RegistrarExpedienteComponent,
    DetalleExpedienteComponent,
    RegistrarParteComponent,
    RegistrarPruebaComponent,
    ListaPartesComponent,
    ListaPruebasComponent,
    DetalleParteComponent,
    DetallePruebaComponent,
    BusquedaExpedienteComponent,
    DatosExpedienteComponent,
    ListaActuadosComponent
  ],
  providers: [
    {
      provide: LocationStrategy,
      useClass: HashLocationStrategy,
    },
    IconSetService,
  ],
  bootstrap: [ AppComponent ]
})
export class AppModule { }

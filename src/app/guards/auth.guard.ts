import { Injectable } from "@angular/core";
import {
  ActivatedRouteSnapshot,
  CanActivate,
  Router,
  RouterStateSnapshot,
  UrlTree,
} from "@angular/router";
import { Observable } from "rxjs";
import { LoginUsuario } from "../models/loginUsuario.model";
import { UsuarioLogged } from "../models/usuarioLogged.model";
import { LoginService } from "../services/login.service";

@Injectable({
  providedIn: "root",
})
export class AuthGuard implements CanActivate {
  loginUsuario: LoginUsuario;
  usuarioLogged: UsuarioLogged
  constructor(private router: Router, private loginService: LoginService) {}

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ):
    | Observable<boolean | UrlTree>
    | Promise<boolean | UrlTree>
    | boolean
    | UrlTree {
    //comprobacion si esta logeado
    if (localStorage.getItem("username") && localStorage.getItem("userpass")) {
      let loginUser = (this.loginUsuario = {
        username: localStorage.getItem("username"),
        userpass: localStorage.getItem("userpass"),
      });
      this.loginService.login(loginUser).subscribe(
        (data) => {
          this.usuarioLogged = data
          if(this.usuarioLogged.resultado){
            return true
          }else{
            return this.router.navigate(["login"]).then(() => false);
          }
        },
        (error) => {
          return this.router.navigate(["login"]).then(() => false);
        }
      );
      /*if (this.loginService.isLogged(loginUser)) {
        return true;
      } else {
        return this.router.navigate(["login"]).then(() => false);
      } */
      return true;
    } else {
      return this.router.navigate(["login"]).then(() => false);
    }
  }
}

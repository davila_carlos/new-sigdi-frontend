import { Component, OnInit } from "@angular/core";
import { navItems } from "../../_nav";
import { nav1 } from "../../navs/_nav1";
import { nav2 } from "../../navs/_nav2";
import { LoginService } from "../../services/login.service";
import { LoginUsuario } from "../../models/loginUsuario.model";
import { UsuarioLogged } from "../../models/usuarioLogged.model";
import { Router } from "@angular/router";

@Component({
  selector: "app-dashboard",
  templateUrl: "./default-layout.component.html",
})
export class DefaultLayoutComponent implements OnInit{
  private usuarioLogged: UsuarioLogged
  private loginUsuario: LoginUsuario
  public sidebarMinimized = false;
  public navItems

  constructor(private loginService: LoginService, private router: Router){

    this.loginUsuario = {
      username: localStorage.getItem('username'),
      userpass: localStorage.getItem('userpass')
    }
    this.loginService.login(this.loginUsuario).subscribe(data=>{
      this.usuarioLogged = data
      switch(this.usuarioLogged.idrol){
        case 1: {
          this.navItems = nav1
          break
        } 
        case 2:{
          this.navItems = nav2
          break
        }
      }
    }, error=>{

    })
  }
  ngOnInit(){
  }
  
  toggleMinimize(e) {
    this.sidebarMinimized = e;
  }

  logOut(){
    localStorage.clear()
    this.router.navigate(['login'])
  }
}

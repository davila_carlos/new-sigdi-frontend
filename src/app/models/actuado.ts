export class Actuado {
    constructor(
      public actuado: string,
      public anulado: boolean,
      public cerrado: boolean,
      public codigousuario: string,
      public fecha: string,
      public hito: string,
      public idactuado: number,
      public idexpediente: number,
      public idseguimientoexpediente: number,
      public idunidad: number,
      public nronotificacion: string,
      public recepcionado: boolean,
      public unidad: string,
    ) {}
  }
  
export class Expediente {

    casillero: boolean
    cerrado: boolean
    codigousuario: string
    despacho: boolean
    estado: number
    fechahoraregistro: string
    id: number
    idlugarsecundario: number
    idmateria: number
    idtipoproceso: number
    idunidad: number
    idunidadseg: number
    nurej: string
    secundario: boolean
    tipopro: number
    tipoproceso: string
    tipounidad: string
    unidad: string

    constructor(
        casillero: boolean,
        cerrado: boolean,
        codigousuario: string,
        despacho: boolean,
        estado: number,
        fechahoraregistro: string,
        id: number,
        idlugarsecundario: number,
        idmateria: number,
        idtipoproceso: number,
        idunidad: number,
        idunidadseg: number,
        nurej: string,
        secundario: boolean,
        tipopro: number,
        tipoproceso: string,
        tipounidad: string,
        unidad: string
    ) {
        this.casillero = casillero
        this.cerrado = cerrado
        this.codigousuario = codigousuario
        this.despacho = despacho
        this.estado = estado
        this.fechahoraregistro = fechahoraregistro
        this.id = id
        this.idlugarsecundario = idlugarsecundario
        this.idmateria = idmateria
        this.idtipoproceso = idtipoproceso
        this.idunidad = idunidad
        this.idunidadseg = idunidadseg
        this.nurej = nurej
        this.secundario = secundario
        this.tipopro = tipopro
        this.tipoproceso = tipoproceso
        this.tipounidad = tipounidad
        this.unidad = unidad
    }

}
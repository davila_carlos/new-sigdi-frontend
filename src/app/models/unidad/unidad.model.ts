import { TipoUnidad } from "./tipounidad.model";

export class Unidad {
  idunidad: number;
  descripcion: string;
  idinstitucion: number;
  iddepartamento: number;
  idlocalidad: number;
  idtipounidad: number;
  estaactivo: boolean;
  tipounidad: TipoUnidad;

  constructor(
    idunidad: number,
    descripcion: string,
    idinstitucion: number,
    iddepartamento: number,
    idlocalidad: number,
    idtipounidad: number,
    estaactivo: boolean,
    tipounidad: TipoUnidad
  ) {
    this.idunidad = idunidad;
    this.descripcion = descripcion;
    this.idinstitucion = idinstitucion;
    this.iddepartamento = iddepartamento;
    this.idlocalidad = idlocalidad;
    this.idtipounidad = idtipounidad;
    this.estaactivo = estaactivo;
    this.tipounidad = tipounidad;
  }
}

export class TipoUnidad {
  idtipounidad: number;
  descripcion: string;
  estaactivo: boolean;

  constructor(idtipounidad: number, descripcion: string, estaactivo: boolean) {
    this.idtipounidad = idtipounidad;
    this.descripcion = descripcion;
    this.estaactivo = estaactivo;
  }
}

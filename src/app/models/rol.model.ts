export class Rol {
  idrol: number;
  descripcion: string;
  fechahoraregistro: Date;
  estaactivo: boolean;

  constructor(
    idrol: number,
    descripcion: string,
    fechahoraregistro: Date,
    estaactivo: boolean
  ) {
    this.idrol = idrol;
    this.descripcion = descripcion;
    this.fechahoraregistro = fechahoraregistro;
    this.estaactivo = estaactivo;
  }
}

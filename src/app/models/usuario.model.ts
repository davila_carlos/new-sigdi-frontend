export class Usuario{
    idusuario: number
    usuario: string
    nombre: string
    paterno: string
    materno: string
    ci: number
    complemento: string
    fechaNacimiento: Date
    email: string
    direccion: string
    telefono: string
    telefonoOficina: string
    estaactivo: boolean

    constructor(
        idusuario: number, 
        usuario: string, 
        nombre: string, 
        paterno: string, 
        materno: string, 
        ci: number, 
        complemento: string, 
        fechaNacimiento: Date, 
        email: string, 
        direccion: string, 
        telefono: string, 
        telefonoOficina: string, 
        estaactivo: boolean
    ) {
        this.idusuario = idusuario
        this.usuario = usuario
        this.nombre = nombre
        this.paterno = paterno
        this.materno = materno
        this.ci = ci
        this.complemento = complemento
        this.fechaNacimiento = fechaNacimiento
        this.email = email
        this.direccion = direccion
        this.telefono = telefono
        this.telefonoOficina = telefonoOficina
        this.estaactivo = estaactivo
      }
}
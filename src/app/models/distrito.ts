export class Distrito {
  constructor(
    public clave: string,
    public descripcion: string,
    public fechahoraregistro: string,
    public id: number,
    public ip: string,
    public nombrebase: string,
    public plataforma: boolean,
    public puerto: string,
    public usuario: string,
  ) {}
}

export class UsuarioLogged{
    idrol?: number
    idunidad?: number
    idusuario?: number
    mensaje?: string
    nombreusuario?: string
    resultado?: boolean
    usuario?: string
    
  constructor(
    idrol?: number, 
    idunidad?: number, 
    idusuario?: number, 
    mensaje?: string, 
    nombreusuario?: string, 
    resultado?: boolean, 
    usuario?: string
) {
    this.idrol = idrol
    this.idunidad = idunidad
    this.idusuario = idusuario
    this.mensaje = mensaje
    this.nombreusuario = nombreusuario
    this.resultado = resultado
    this.usuario = usuario
  }

}
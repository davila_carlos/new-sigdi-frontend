import { HttpClient, HttpHeaders } from '@angular/common/http'
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs'

@Injectable({
  providedIn: 'root'
})
export class ExpedienteService {

  constructor(private httpClient: HttpClient) { }

  getExpediente(body):Observable<any>{
    const headers = new HttpHeaders({
      'Authorization': 'Bearer XXX',
      'usr': 'ALEX',
      'Content-Type': 'application/json'
    });
/*     let body = {
      "nurej": "1036209",
       "ip": "192.168.13.7",
       "puerto": "5432",
      "razon": "Disciplinario",
      "idusuario":-999
      } */
    return this.httpClient.post<any>('/api/distrito/expedienteBearerControl', body, { headers });
  }
}

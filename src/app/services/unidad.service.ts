import { HttpClient } from "@angular/common/http"
import { Injectable } from "@angular/core"
import { Observable } from "rxjs"
import { environment } from "../../environments/environment"

@Injectable({
  providedIn: "root",
})
export class UnidadService {
  url: string = environment.URL_API + "unidad";
  constructor(private httpClient: HttpClient) { }

  getUnidades(): Observable<any> {
    return this.httpClient.get(this.url)
  }
}

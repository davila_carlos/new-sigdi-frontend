import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class UsuarioService {
  url: string = environment.URL_API + 'usuario'

  constructor(private httpClient: HttpClient) { }

  getUsuarios():Observable<any>{
    return this.httpClient.get(this.url)
  }
}

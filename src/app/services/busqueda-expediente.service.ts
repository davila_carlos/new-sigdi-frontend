import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { environment } from "../../environments/environment";
import { Observable } from "rxjs";

@Injectable({
  providedIn: "root",
})
export class BusquedaExpedienteService {
  public url: string;

  constructor(private _httpClient: HttpClient) {
    this.url = environment.URL_API;
  }

  getDistritos(): Observable<any> {
    const headers = new HttpHeaders({
      Host: "psirej.organojudicial.gob.bo",
      Authorization: "Bearer XXX",
      usr: "ALEX",
      "Cache-Control": "no-cache",
      "Content-Type": "application/json",
      "Access-Control-Allow-Origin": "*",
    });
    return this._httpClient.post<any>(
      "/api/cm/listadistritosbearerall",
      {},
      { headers: headers }
    );
  }

  getPartes(id: number): Observable<any> {
    const headers = new HttpHeaders({
      Host: "psirej.organojudicial.gob.bo",
      Authorization: "Bearer XXX",
      usr: "ALEX",
      "Cache-Control": "no-cache",
      "Content-Type": "application/json",
      "Access-Control-Allow-Origin": "*",
    });
    const body = {
      idexpediente: id,
      ip: "192.168.13.7",
    };
    return this._httpClient.post<any>("/api/distrito/partesBearer", body, {
      headers: headers,
    });
  }

  getActuado(body) {
    const headers = new HttpHeaders({
      'Host': 'psirej.organojudicial.gob.bo',
      'Authorization': 'Bearer XXX',
      'usr': 'ALEX',
      'Cache-Control': 'no-cache',
      'Content-Type': 'application/json',
      'Access-Control-Allow-Origin': '*'
    });
    
    return this._httpClient.post<any>('/api/distrito/seguimientoexpBearer', body, { headers: headers });
  }
}

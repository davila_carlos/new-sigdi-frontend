import { HttpClient } from '@angular/common/http'
import { Injectable } from '@angular/core'
import { LoginUsuario } from '../models/loginUsuario.model'
import { UsuarioLogged } from '../models/usuarioLogged.model'
import { environment } from '../../environments/environment'

@Injectable({
  providedIn: 'root'
})
export class LoginService {
  usuarioLogged: UsuarioLogged
  url: string = environment.URL_API + 'usuario/logueo'

  constructor(private httpClient: HttpClient) { }

  login(datosLogin: LoginUsuario) {
    return this.httpClient.post(this.url, datosLogin)
  }

  isLogged(datosLogin: LoginUsuario): boolean {
    let logged: boolean
    this.httpClient.post(this.url, datosLogin).subscribe(data => {
      console.log(data)
      this.usuarioLogged = data
      if (this.usuarioLogged.resultado) {
        logged = true
      } else {
        logged = false
      }
    }, error => {
      console.log(error)
      logged = false
    })
    return logged
  }
}

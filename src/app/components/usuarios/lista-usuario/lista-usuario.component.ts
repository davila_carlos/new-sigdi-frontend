import { Component, OnInit } from '@angular/core';
import { Usuario } from '../../../models/usuario.model';
import { UsuarioService } from '../../../services/usuario.service';

@Component({
  selector: 'app-lista-usuario',
  templateUrl: './lista-usuario.component.html',
  styleUrls: ['./lista-usuario.component.scss']
})
export class ListaUsuarioComponent implements OnInit {
  listaUsuarios: Usuario[] = []
  constructor(private usuarioService: UsuarioService) { }

  ngOnInit(): void {
    this.obtenerUsuarios()
  }

  obtenerUsuarios(){
    this.usuarioService.getUsuarios().subscribe(data=>{
      console.log(data.data[0])
      this.listaUsuarios = data.data
      console.log('--------------------')
      console.log(this.listaUsuarios);
    }, error =>{
      console.log(error);
    })
  }

}

import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { ToastrService } from "ngx-toastr";

@Component({
  selector: "app-cambio-password-usuario",
  templateUrl: "./cambio-password-usuario.component.html",
  styleUrls: ["./cambio-password-usuario.component.scss"],
})
export class CambioPasswordUsuarioComponent implements OnInit {
  typePasswordActual: boolean = false;
  typePasswordNuevo: boolean = false;
  typePasswordConfirm: boolean = false;
  equalPassword: boolean = false;

  cambioClaveForm: FormGroup;
  constructor(private fb: FormBuilder, private toastr: ToastrService) {
    this.cambioClaveForm = this.fb.group({
      passwordActual: ["", Validators.required],
      passwordNuevo: ["", Validators.required],
      passwordConfirm: ["", Validators.required],
    });
  }

  ngOnInit(): void {}

  cambioClave() {
    if (!this.cambioClaveForm.invalid) {
      if (
        this.cambioClaveForm.get("passwordNuevo").value ===
        this.cambioClaveForm.get("passwordConfirm").value
      ){
        this.equalPassword = false
        this.toastr.success('Contraseña reseteada con exito!', 'Correcto')
      }else{
        this.equalPassword = true
        this.toastr.info('La contraseña no es identica', 'Error')
      }
    } else {
      Object.keys(this.cambioClaveForm.controls).forEach((field) => {
        // {1}
        const control = this.cambioClaveForm.get(field) as any; // {2}
        control.markAsTouched({ onlySelf: true }); // {3}
      });
    }
  }

  togglePasswordActual() {
    this.typePasswordActual = !this.typePasswordActual;
  }
  togglePasswordNuevo() {
    this.typePasswordNuevo = !this.typePasswordNuevo;
  }
  togglePasswordConfirm() {
    this.typePasswordConfirm = !this.typePasswordConfirm;
  }
}

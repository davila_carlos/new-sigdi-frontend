import { Component, OnChanges, OnInit } from "@angular/core"
import { FormBuilder, FormGroup, Validators } from "@angular/forms"
import { MatDatepickerModule } from "@angular/material/datepicker"
import { Router } from "@angular/router"
import { ToastrService } from "ngx-toastr"
import { Rol } from "../../../models/rol.model"
import { Unidad } from "../../../models/unidad/unidad.model"
import { RolService } from "../../../services/rol.service"
import { UnidadService } from "../../../services/unidad.service"
@Component({
  selector: "app-registrar-usuario",
  templateUrl: "./registrar-usuario.component.html",
  styleUrls: ["./registrar-usuario.component.scss"],
})
export class RegistrarUsuarioComponent implements OnInit {
  registrarUsuarioForm: FormGroup
  roles: Rol[] = []
  unidades: Unidad[] = []
  unidadesFiltradas: Unidad[] = []
  constructor(
    private fb: FormBuilder,
    private router: Router,
    private toastr: ToastrService,
    private rolService: RolService,
    private unidadService: UnidadService
  ) {
    this.registrarUsuarioForm = this.fb.group({
      rol: ["", Validators.required],
      distrito: ["", Validators.required],
      unidad: ["", Validators.required],
      nombre: ["", Validators.required],
      apellidoPaterno: ["", Validators.required],
      apellidoMaterno: ["", Validators.required],
      fechaDeNacimiento: ["", Validators.required],
      ci: ["", Validators.required],
      complemento: ["", Validators.required],
      expedido: ["", Validators.required],
      correo: [
        "",
        [
          Validators.required,
          Validators.email,
          Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$"),
        ],
      ],
      direccion: ["", Validators.required],
      celular: ["", Validators.required],
      telefonoOficina: [""],
    })
  }

  ngOnInit(): void {
    this.getRoles()
    this.getUnidades()
  }

  registrarUsuario() {
    if (this.registrarUsuarioForm.valid) {
      this.toastr.success(
        "Usuario registrado con exito!",
        "Usuario registrado"
      )
    } else {
      Object.keys(this.registrarUsuarioForm.controls).forEach((field) => {
        // {1}
        const control = this.registrarUsuarioForm.get(field) as any // {2}
        control.markAsTouched({ onlySelf: true }) // {3}
      })
      this.toastr.warning(
        "Por favor, revise los datos del formulario!",
        "Error"
      )
    }
  }
  //filtro
  unidadFiltro(value) {
    this.unidadesFiltradas = this.unidades.filter(unidad=>unidad.iddepartamento == value)
  }

  //peticiones
  getUnidades() {
    this.unidadService.getUnidades().subscribe(data => {
      this.unidades = data.data
      console.log(this.unidades);
      let value = this.registrarUsuarioForm.get('distrito').value
      this.unidadesFiltradas = this.unidades.filter(unidad=>unidad.iddepartamento == value)
      this.registrarUsuarioForm.get("distrito").patchValue(0);
    }, error => {
    })
  }
  getRoles() {
    this.rolService.getRoles().subscribe(
      (data) => {
        this.roles = data.data
        console.log(this.roles)
      },
      (error) => { }
    )
  }
}

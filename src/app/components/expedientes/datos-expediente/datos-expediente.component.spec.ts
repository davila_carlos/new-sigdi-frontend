import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DatosExpedienteComponent } from './datos-expediente.component';

describe('DatosExpedienteComponent', () => {
  let component: DatosExpedienteComponent;
  let fixture: ComponentFixture<DatosExpedienteComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DatosExpedienteComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DatosExpedienteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

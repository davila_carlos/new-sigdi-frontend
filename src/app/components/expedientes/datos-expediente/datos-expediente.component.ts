import { Component, Input, OnChanges, OnInit, Output } from '@angular/core'
import { ToastrService } from 'ngx-toastr'
import { Expediente } from '../../../models/expediente/expediente.model'
import { ExpedienteService } from '../../../services/expediente.service'

@Component({
  selector: 'app-datos-expediente',
  templateUrl: './datos-expediente.component.html',
  styleUrls: ['./datos-expediente.component.scss']
})
export class DatosExpedienteComponent implements OnInit,OnChanges{
  @Input('expediente') expediente: Expediente
  constructor(private expedienteService: ExpedienteService, private toastr: ToastrService) { }

  ngOnInit(): void {

  }
  ngOnChanges():void{
    this.busqueda()
  }

  busqueda(){

  }
}

import { Component, OnInit } from "@angular/core"
import { FormBuilder, FormGroup, Validators } from "@angular/forms"
import { ToastrService } from "ngx-toastr"
import { Distrito } from "../../../models/distrito"
import { Expediente } from "../../../models/expediente/expediente.model"
import { BusquedaExpedienteService } from "../../../services/busqueda-expediente.service"
import { ExpedienteService } from "../../../services/expediente.service"

@Component({
  selector: "app-busqueda-expediente",
  templateUrl: "./busqueda-expediente.component.html",
  styleUrls: ["./busqueda-expediente.component.scss"],
})
export class BusquedaExpedienteComponent implements OnInit {

  detalle: boolean = false;
  caratula: boolean = false;
  expediente: boolean = true;
  actuado: boolean = false;
  formBusquedaExpediente: FormGroup
  listaDistritos: Distrito[] = [];
  nurej: string
  distrito: string
  motivo_razon: string
  expedienteObjeto: Expediente

  constructor(
    private fb: FormBuilder,
    private toastr: ToastrService,
    private busquedaExpedienteService: BusquedaExpedienteService,
    private expedienteService: ExpedienteService
  ) { }

  ngOnInit(): void {
    this.formBusquedaExpediente = this.fb.group({
      distrito: ["", Validators.required],
      nurej: ["", Validators.required],
      motivo: ["", Validators.required],
    })
    this.listarDistritos()
  }

  busqueda() {
    if (!this.formBusquedaExpediente.invalid) {
      /*       alert(this.formBusquedaExpediente.get('distrito').value+this.formBusquedaExpediente.get('nurej').value+this.formBusquedaExpediente.get('motivo').value) */
      //this.toastr.success("Expediente encontrado", "Correcto");
      this.nurej = this.formBusquedaExpediente.get("nurej").value
      this.distrito = this.formBusquedaExpediente.get("distrito").value
      this.motivo_razon = this.formBusquedaExpediente.get("motivo").value
      this.busquedaExpediente()
      this.caratula = true
    } else {
      Object.keys(this.formBusquedaExpediente.controls).forEach((field) => {
        // {1}
        const control = this.formBusquedaExpediente.get(field) as any // {2}
        control.markAsTouched({ onlySelf: true }) // {3}
      })
      this.toastr.error("Revise el formulario", "Error")
    }
  }

  busquedaExpediente() {
    let body = {
      "nurej": this.nurej,
      "ip": this.distrito,
      "puerto": "5432",
      "razon": this.motivo_razon,
      "idusuario": -999
    }

    if (this.distrito == '192.168.13.7' || this.distrito == '192.168.54.86') {
      this.expedienteService.getExpediente(body).subscribe(data => {
        console.log(data)
        if (data.id == -999) {
          this.toastr.error('Expediente no encontrado', 'Error')
          this.expedienteObjeto = undefined
        }
        else {
          this.toastr.success('Expediente encontrado', 'Correcto')
          this.expedienteObjeto = data
        }
      }, error => {
        console.log(error)
      })
    }

    else {
      this.expediente = undefined
      this.toastr.error('Expediente no encontrado', 'Error')
    }

  }

  expedienteTab() {
    this.expediente = true
    this.actuado = false
  }

  actuadoTab() {
    this.expediente = false
    this.actuado = true
  }

  listarDistritos(): void {
    this.busquedaExpedienteService.getDistritos().subscribe(
      (data) => {
        this.listaDistritos = data
      },
      (error) => {
        console.log(`Some error happend: ${error.message}`)
      }
    )
  }
}

import { Component, OnInit } from '@angular/core'
import { FormBuilder, FormGroup, Validators } from '@angular/forms'
import { Router } from '@angular/router'
import { ToastrService } from 'ngx-toastr'

@Component({
  selector: 'app-registrar-expediente',
  templateUrl: './registrar-expediente.component.html',
  styleUrls: ['./registrar-expediente.component.scss']
})
export class RegistrarExpedienteComponent implements OnInit {
  registroExpedienteForm: FormGroup
  constructor(private fb: FormBuilder, private toastr: ToastrService, private router: Router) { }

  ngOnInit(): void {
    this.registroExpedienteForm = this.fb.group({
      tipoMedidaPrecautoria: ['', Validators.required],
      unidad: ['', Validators.required],
      relatoHechos: ['', Validators.required],
      fechaIngresoDenuncia: ['', Validators.required],
      numeroDeFojas: ['', Validators.required],
    })
  }

  registrarDenuncia() {
    if (this.registroExpedienteForm.valid) {
      this.toastr.success('Denuncia registrada con exito!', 'Registrado')
      this.router.navigate(['expedientes/lista'])
    } else {
      Object.keys(this.registroExpedienteForm.controls).forEach((field) => {
        // {1}
        const control = this.registroExpedienteForm.get(field) as any // {2}
        control.markAsTouched({ onlySelf: true }) // {3}
      })
      this.toastr.error('Hay un error', 'Error')
    }
  }
}

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DetallePruebaComponent } from './detalle-prueba.component';

describe('DetallePruebaComponent', () => {
  let component: DetallePruebaComponent;
  let fixture: ComponentFixture<DetallePruebaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DetallePruebaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DetallePruebaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RegistrarPruebaComponent } from './registrar-prueba.component';

describe('RegistrarPruebaComponent', () => {
  let component: RegistrarPruebaComponent;
  let fixture: ComponentFixture<RegistrarPruebaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RegistrarPruebaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RegistrarPruebaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

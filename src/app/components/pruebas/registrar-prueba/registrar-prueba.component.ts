import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms'
import { ToastrService } from 'ngx-toastr'

@Component({
  selector: 'app-registrar-prueba',
  templateUrl: './registrar-prueba.component.html',
  styleUrls: ['./registrar-prueba.component.scss']
})
export class RegistrarPruebaComponent implements OnInit {
  registroPruebaForm: FormGroup
  constructor(private fb: FormBuilder, private toastr: ToastrService) { }

  ngOnInit(): void {
    this.registroPruebaForm = this.fb.group({
      numeroFojas: ['', Validators.required],
      adjuntarArchivo: ['', Validators.required],
      pruebaSe: ['', Validators.required]
    })
    this.registroPruebaForm.patchValue({pruebaSe: 'true'})
  }

  registrarPrueba(){
    if(this.registroPruebaForm.valid){
      this.toastr.success('El registro de la prueba fue exitoso', 'Registrado')
    }else{
      Object.keys(this.registroPruebaForm.controls).forEach((field) => {
        // {1}
        const control = this.registroPruebaForm.get(field) as any // {2}
        control.markAsTouched({ onlySelf: true }) // {3}
      })
      console.log(this.registroPruebaForm)
      this.toastr.error('No se pudo registrar la prueba!', 'error')
    }
  }
}

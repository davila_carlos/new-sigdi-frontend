import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms'
import { ToastrService } from 'ngx-toastr'

@Component({
  selector: 'app-registrar-parte',
  templateUrl: './registrar-parte.component.html',
  styleUrls: ['./registrar-parte.component.scss']
})
export class RegistrarParteComponent implements OnInit {
  registrarParteForm: FormGroup
  constructor(private fb : FormBuilder,private toastr: ToastrService) { }

  ngOnInit(): void {
    this.registrarParteForm = this.fb.group({
      nombreParte: ['', Validators.required]
    })
  }

  registrarParte(){
    if(this.registrarParteForm.valid){
      this.toastr.success('Se registro la parte correctamente!', 'Registro')
    }else{
      Object.keys(this.registrarParteForm.controls).forEach((field) => {
        // {1}
        const control = this.registrarParteForm.get(field) as any // {2}
        control.markAsTouched({ onlySelf: true }) // {3}
      })
      this.toastr.error('No se pudo registrar la parte!', 'error')
    }
  }
}

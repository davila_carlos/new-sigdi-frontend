import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RegistrarParteComponent } from './registrar-parte.component';

describe('RegistrarParteComponent', () => {
  let component: RegistrarParteComponent;
  let fixture: ComponentFixture<RegistrarParteComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RegistrarParteComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RegistrarParteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

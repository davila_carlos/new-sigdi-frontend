import { Input } from "@angular/core"
import { Component, OnInit, OnChanges } from "@angular/core"
import { Expediente } from "../../../models/expediente/expediente.model"
import { Parte } from "../../../models/parte"
import { BusquedaExpedienteService } from "../../../services/busqueda-expediente.service"
@Component({
  selector: "app-lista-partes",
  templateUrl: "./lista-partes.component.html",
  styleUrls: ["./lista-partes.component.scss"],
})
export class ListaPartesComponent implements OnInit, OnChanges {
  @Input("expediente") expediente: Expediente
  listaPartes: Parte[] = [];

  constructor(private busquedaService: BusquedaExpedienteService) { }

  ngOnInit(): void {
    if (this.expediente) {
      this.busquedaService
        .getPartes(this.expediente.id)
        .subscribe((data) => (this.listaPartes = data))
    }

  }

  ngOnChanges(): void {
    if (this.expediente) {
      this.busquedaService
        .getPartes(this.expediente.id)
        .subscribe((data) => (this.listaPartes = data))
    }

  }
}

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListaActuadosComponent } from './lista-actuados.component';

describe('ListaActuadosComponent', () => {
  let component: ListaActuadosComponent;
  let fixture: ComponentFixture<ListaActuadosComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListaActuadosComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ListaActuadosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

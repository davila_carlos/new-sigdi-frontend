import { Component, OnInit } from '@angular/core';

import { BusquedaExpedienteService } from '../../../services/busqueda-expediente.service';
import { Actuado } from '../../../models/actuado';
import { Input } from '@angular/core';
import { Expediente } from '../../../models/expediente/expediente.model'

@Component({
  selector: 'app-lista-actuados',
  templateUrl: './lista-actuados.component.html',
  styleUrls: ['./lista-actuados.component.scss']
})
export class ListaActuadosComponent implements OnInit {

  @Input('expediente') expediente: Expediente;
  @Input('direccion') direccion: string
  listaActuados: Actuado[] = [];

  constructor(
    private busquedaExpedienteService: BusquedaExpedienteService,
  ) { }

  ngOnInit(): void {
    this.listarActuado();
  }

  listarActuado(): void {
    const body = {
      "idexpediente": this.expediente.id,     // id del expediente
      "idunidad": this.expediente.idunidad,       // id de la unidad
      "direccion": this.direccion // ip del server 
    };

    this.busquedaExpedienteService.getActuado(body).subscribe(
      data => {
        this.listaActuados = data;
        console.log(this.listaActuados);
      },
      error => {
        console.log(error)
      }
    )
  }
}

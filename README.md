# NUEVO FRONT END SIGDI 💻
## TABLA DE CONTENIDO :page_with_curl:
- [Tabla de contenido](#tabla-de-contenido)
    - [Requisitos](#requisitos)
    - [Instalación](#instalacion)

### Requisitos
- [NodeJS - Version14.17.4 LTS](https://nodejs.org/es/) :alien:
- [Editor de Texto - Ejemplo (Visual Studio Code)](https://code.visualstudio.com/) :blue_book:
- [Angular - CLI]() :keyboard:
    
        npm install -g @angular/cli
- [PostgreSQL 13](https://www.postgresql.org/about/news/postgresql-13-released-2077/) :elephant:

- [Git](https://git-scm.com/)

### Instalacion 
1. Clonar el sistema del repositorio de GitLab
        
        git clone https://gitlab.com/davila_carlos/new-sigdi-frontend.git

2. Instalar modulos de nodeJs

        npm install

3. Modificar el archivo src\environments\environment.ts con la url del servidor backend

        export const environment = {
            production: false,
            URL_API: 'http://192.168.10.231:3000/api/'
            //URL_API: 'http://localhost:3000/api/'
        };

4. Iniciar el servidor 

        npm start

5. nota: (Se debe contar con conexion a internet) :electric_plug:
